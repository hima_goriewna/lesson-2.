package com.example.hima.lesson2;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class ListenersActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "States";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listeners);
        Button buttonRed = (Button) findViewById(R.id.button_red);
        buttonRed.setOnClickListener(this);

        Button buttonGreen = (Button) findViewById(R.id.button_green);
        buttonGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "Нажата зелёная кнопка");
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_red) {
            Log.e(TAG, "Нажата красная кнопка");
        }
    }
}
